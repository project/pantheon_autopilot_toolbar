<?php

namespace Drupal\pantheon_autopilot_toolbar\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\TrustedRedirectResponse;

class PantheonAutopilotToolbarController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function sendToPantheon() {
    return new TrustedRedirectResponse('https://dashboard.pantheon.io/site/' . $_ENV["PANTHEON_SITE"] . '/autopilot/status');
  }

}