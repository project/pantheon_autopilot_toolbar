<?php

namespace Drupal\pantheon_autopilot_toolbar;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Toolbar integration handler.
 */
class ToolbarHandler implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * ToolbarHandler constructor.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   *   The current user.
   */
  public function __construct(AccountProxyInterface $account) {
    $this->account = $account;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user')
    );
  }

  /**
   * Hook bridge.
   *
   * @return array
   *   The devel toolbar items render array.
   *
   * @see hook_toolbar()
   */
  public function toolbar() {

    $items['pantheon_autopilot_toolbar'] = [
      '#cache' => [
        'contexts' => ['user.permissions'],
      ],
    ];

    if ($this->account->hasPermission('pantheon autopilot access')) {
      $items['pantheon_autopilot_toolbar'] += [
        '#type' => 'toolbar_item',
        '#weight' => 999,
        'tab' => [
          '#type' => 'link',
          '#title' => $this->t('Autopilot'),
          '#url' => Url::fromRoute('pantheon_autopilot_toolbar.redirect'),
          '#attributes' => [
            'title' => $this->t('Autopilot'),
            'class' => ['toolbar-icon', 'toolbar-icon-pantheon-autopilot'],
          ],
        ],
        '#attached' => [
          'library' => 'pantheon_autopilot_toolbar/pantheon-autopilot-toolbar',
        ],
      ];
    }

    return $items;
  }

}
