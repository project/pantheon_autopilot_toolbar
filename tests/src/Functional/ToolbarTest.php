<?php

namespace Drupal\Tests\rebuild_cache_access\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests rebuild_cache_access toolbar functionality.
 *
 * @group rebuild_cache_access
 */
class ToolbarTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  public static $modules = ['toolbar', 'pantheon_autopilot_toolbar'];

  /**
   * Tests that the toolbar button works.
   */
  public function testButton() {

    // First, a user without rebuild cache permission should not see the button.
    $editor = $this->drupalCreateUser([
      'access toolbar',
    ]);
    $this->drupalLogin($editor);

    $this->drupalGet('');
    $this->assertSession()->pageTextNotContains('Autopilot');

    // Second, a user with rebuild cache permission should see the button.
    $developer = $this->drupalCreateUser([
      'access toolbar',
      'pantheon autopilot access',
    ]);
    $this->drupalLogin($developer);

    $this->drupalGet('');
    $this->assertSession()->pageTextContains('Autopilot');

    // Click the button.
    $this->clickLink('Autopilot');
    $this->assertSession()->pageTextContains('Autopilot');
  }

}
